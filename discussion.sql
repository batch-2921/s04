-- add new artist records artists(8,9,10,11)
INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO artists (name) VALUES ("Justin Bieber");
INSERT INTO artists (name) VALUES ("Ariana Grande");
INSERT INTO artists (name) VALUES ("Bruno Mars");

-- add new songs in fearless album songs(6,7)
INSERT INTO songs (song_name, length, genre, album_id) 
VALUES ("Fearless", "00:04:06", "Pop rock", 2),
       ("Love Story", "00:03:33", "Country pop", 2);

-- add new album taylor swift album(7)
INSERT INTO albums (album_title, date_released, artist_id) 
VALUES ("Red", "2012-10-22", 5);

-- add song in red album songs(8,9)
INSERT INTO songs (song_name, length, genre, album_id) 
VALUES ("State of Grace", "00:04:33", "Rock, Alternative rock", 7);
INSERT INTO songs (song_name, length, genre, album_id) 
VALUES ("Red", "00:03:24", "Country", 7);

-- lady gaga albums(8)
INSERT INTO albums (album_title, date_released, artist_id) 
VALUES ("A Star Is Born", "2018-10-05", 8);
INSERT INTO albums (album_title, date_released, artist_id) 
VALUES ("Born this way", "2011-05-23", 8);
-- lady gaga songs(10,11,12)
INSERT INTO songs (song_name, length, genre, album_id) 
VALUES ("Black Eyes", "00:03:01", "Rock and roll", 8),
       ("Shallow", "00:03:21", "Country, rock and folk rock", 8);
INSERT INTO songs (song_name, length, genre, album_id) 
VALUES ("Born this way", "00:04:12", "Electropop", 9);