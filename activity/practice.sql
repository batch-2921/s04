CREATE TABLE users(
  id INT NOT NULL AUTO_INCREMENT,
  username VARCHAR(50) NOT NULL,
  password VARCHAR(50) NOT NULL,
  full_name VARCHAR(50) NOT NULL,
  contact_number INT NOT NULL,
  email VARCHAR(50),
  address VARCHAR(50),
  PRIMARY KEY (id)
);

INSERT INTO users (username, password, full_name, contact_number, email, address) 
VALUES ("Rimuru", "passwordA", "Rimuru Tempest", 1234567891, "rimuru@gmail.com", "Jura Tempest Federation"),
       ("Ains", "passwordB", "Ains Oal Gown", 1234567891, "ains@gmail.com", "Great Tomb of Nazarick"),
       ("Teresa", "passwordC", "Teresa Faint Smile", 1234567891, "teresa@gmail.com", "Unknown"),
       ("Saitama", "passwordD", "Saitama", 1234567891, "saitama@gmail.com", "City Z"),
       ("Albedo", "passwordE", "Albedo Arubedo", 1234567891, "albedo@gmail.com", "Great Tomb of Nazarick");

CREATE TABLE reviews (
    id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    review VARCHAR(500),
    datetime_created DATETIME,
    rating INT,
    PRIMARY KEY (id),
    CONSTRAINT fk_reviews_user_id FOREIGN KEY (user_id) REFERENCES users(id)
);

INSERT INTO reviews (user_id, review, datetime_created, rating) 
VALUES (1, "The songs are okay. Worth the subscription", "2023-05-03 00:00:00", 5),
       (2, "The songs are meh. I want BLACKPINK", "2023-01-23 00:00:00", 1),
       (3, "Add Bruno Mars and Lady Gaga", "2023-03-23 00:00:00", 4),
       (4, "I want to listen to more k-pop", "2022-09-23 00:00:00", 3),
       (5, "Kindly add more OPM", "2023-02-01 00:00:00", 5);

-- i dont have users with k so i changed it to o instead.
SELECT * FROM users JOIN reviews ON users.id = reviews.user_id WHERE full_name LIKE "%o%";
-- i dont have users with x so i changed it to r instead.
SELECT * FROM users JOIN reviews ON users.id = reviews.user_id WHERE full_name LIKE "%r%";

SELECT * FROM reviews JOIN users ON reviews.id = users.id;

SELECT review, username FROM reviews JOIN users ON reviews.id = users.id;